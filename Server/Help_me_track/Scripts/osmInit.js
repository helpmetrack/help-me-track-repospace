﻿var map = null;
var stylePoint = new OpenLayers.Style(
{
    pointRadius: 20,
    strokeColor: "red",
    strokeWidth: 1,
    fillColor: "#22FF00",
    labelYOffset: -16,
    label: "${label}",
    fontSize: 16
});
var vectorPoint = new OpenLayers.Layer.Vector("Транспорт",
{
    styleMap: new OpenLayers.StyleMap(
    { "default": stylePoint,
      "select": { pointRadius: 20}
    })
});
  function init() {
    map = new OpenLayers.Map("basicMap");
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);
    map.setCenter(new OpenLayers.LonLat(46.020543089495426, 51.53170909793143) // Центр карты
      .transform(
        new OpenLayers.Projection("EPSG:4326"), // преобразование из WGS 1984
        new OpenLayers.Projection("EPSG:900913") // в Spherical Mercator Projection
      ), 14 // Уровень масштаба
    );
    map.addControl(new OpenLayers.Control.ScaleLine());
    map.addControl(new OpenLayers.Control.MousePosition());
    map.addLayer(vectorPoint);
    console.log(map);
  };
function addPoint(lon,lat,title,ident){
    console.log(map);
    var ttt = new OpenLayers.LonLat(parseFloat(lon), parseFloat(lat));
    ttt.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
    for (var k = 0; k < map.layers[1].features.length; k++)
    {
    if(map.layers[1].features[k].attributes.PointId==ident) {
            map.layers[1].features[k].attributes.label=title;
            map.layers[1].features[k].move(ttt);
            return false;

        }
    }
    var point0 = new OpenLayers.Geometry.Point(parseFloat(lon), parseFloat(lat));
    point0.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
    map.layers[1].addFeatures(new OpenLayers.Feature.Vector(point0, { label: title, name: title, PointId: ident }));
};

function changeFillColor(color){
    stylePoint = new OpenLayers.Style(
    {
        pointRadius: 5,
        strokeColor: "blue",
        strokeWidth: 1,
        fillColor: color,
        labelYOffset: -16,
        label: "${label}",
        fontSize: 16
    });
    map.layers[1].styleMap = new OpenLayers.StyleMap(
        { "default": stylePoint,
          "select": { pointRadius: 20}
        });
};
function addTransport(lon,lat,title,ident, load){
    var color = "00";
    if (load <= 50){
        var value_red = (5*load).toString(16).toUpperCase();
        if (value_red.length == 1)
            value_red = "0" + value_red
        color = "#"+value_red+"FF00";

    }  else {
        var value_green  = (5*(100-load)).toString(16).toUpperCase();
        if (value_green.length == 1)
            value_green = "0" + value_green
        color = "#FF"+value_green+"00";
    }
    changeFillColor(color);
    addPoint(lon, lat, title + " Load:" + load, ident)
}

function update_all_transport(){
    var url = "/Transport";
//    url = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent('select * from html where url="' + url + '"') + '&format=xml';
    $.ajax({
     url: url,
     dataType: "json",
//     dataType: "text",
     success:function(json){
//         json = JSON.parse(json.split("<p>")[1].split("</p>")[0]);
         console.log(json);
         $("#selectable").empty();
         for (var num in json.result){
             var trans = json.result[num];
             addTransport(parseFloat(trans.Lon), parseFloat(trans.Lat),
                     "%type %track".replace("%type", trans.Type).replace("%track", trans.Track),
                     parseInt(trans.Num), parseInt(trans.Load));
             $("#selectable").append('<li class="ui-widget-content">%type %track Load: %load</li>'
                 .replace("%type", trans.Type).replace("%track", trans.Track).replace("%load", trans.Load));
         }

     }

    });
}
