﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Help_me_track.Models
{
    public class Transport
    {
        public int id { get; set; }
        public String Track { get; set; }
        public String Type { get; set; }
        public String Num { get; set; }
        public String Lat { get; set; }
        public String Lon { get; set; }
        public int Load { get; set; }
    }
    public class TransportTime
    {
        public int id { get; set; }
        public String Track { get; set; }
        public String Type { get; set; }
        public String Num { get; set; }
        public String Lat { get; set; }
        public String Lon { get; set; }
        public int Load { get; set; }
        public DateTime date { get; set; }
    }
    public class TransportDBContext : DbContext
    {
        public DbSet<Transport> Transport { get; set; }
    }
    public class TransportTimeDBContext : DbContext
    {
        public DbSet<TransportTime> Transport { get; set; }
    }
}