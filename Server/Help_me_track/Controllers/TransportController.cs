﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Help_me_track.Models;
using System.Data.Entity.Infrastructure;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Help_me_track.Controllers
{ 
    public class TransportController : Controller
    {
        private TransportDBContext db = new TransportDBContext();
        private TransportTimeDBContext tt = new TransportTimeDBContext();

        //
        // GET: /Transport/

        public ActionResult Index()
        {
            //return View(db.Transport.ToList());
            Dictionary<String, Object> result = new Dictionary<string, object>();
            result.Add("status", "OK");
            result.Add("result", db.Transport.ToList());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult All()
        {
            return View("Index",db.Transport.ToList());
        }

        //
        // GET: /Transport/Track/5

        public ActionResult Track(int id)
        {
            Transport transport = db.Transport.Find(id);
            DbSqlQuery<Transport> query = db.Transport.SqlQuery("SELECT * FROM Transports WHERE Transports.Track=" + id.ToString());
            if (query.Count() > 0)
            {
                Dictionary<String, Object> result = new Dictionary<string, Object>();
                result.Add("status", "OK");
                result.Add("result", query.ToList());
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Dictionary<String,String> result = new Dictionary<string,string>();
                Response.Headers.Add("Content-type", "application/json");
                result.Add("status", "Bad");
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Transport/Create

        public ActionResult Create()
        {
            return View();
        } 

        
        [HttpPost]
        public String Add(Transport transport)
        {
            StreamReader reader = new StreamReader(Request.InputStream);
            JObject content = JObject.Parse(reader.ReadToEnd());
            if (ModelState.IsValid)
            {
                transport.id = int.Parse(content.GetValue("ID").ToString());
                transport.Num = content.GetValue("ID").ToString();
                transport.Lat = content.GetValue("LAT").ToString();
                transport.Lon = content.GetValue("LON").ToString();
                transport.Track = content.GetValue("number").ToString();
                transport.Load = int.Parse(content.GetValue("load").ToString());
                transport.Type = content.GetValue("type").ToString();
                DbSqlQuery<Transport> query = db.Transport.SqlQuery("SELECT * FROM Transports WHERE Transports.Num=" + transport.Num);
                if (query.Count() > 0)
                {
                    Transport toReplace = query.ToList()[0];
                    db.Transport.Remove(toReplace);
                    db.Transport.Add(transport);
                }
                else
                {
                    db.Transport.Add(transport);
                }
                db.SaveChanges();
                return "result: OK";
            }

            return "result: BAD";
        }

        //
        // POST: /Transport/Create
        [HttpPost]
        public ActionResult Create(Transport transport)
        {
            if (ModelState.IsValid)
            {
                db.Transport.Add(transport);
                db.SaveChanges();
                return RedirectToAction("All");  
            }

            return View(transport);
        }
        
        //
        // GET: /Transport/Edit/5
 
        public ActionResult Edit(int id)
        {
            Transport transport = db.Transport.Find(id);
            return View(transport);
        }

        //
        // POST: /Transport/Edit/5

        [HttpPost]
        public ActionResult Edit(Transport transport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transport);
        }

        //
        // GET: /Transport/Delete/5
 
        public ActionResult Delete(int id)
        {
            Transport transport = db.Transport.Find(id);
            return View(transport);
        }

        //
        // POST: /Transport/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Transport transport = db.Transport.Find(id);
            db.Transport.Remove(transport);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}