﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System.Text;
using System.IO;

namespace Help_me_track.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Map");
        }

        public ActionResult About()
        {
            String[] team = System.IO.File.ReadAllLines(Server.MapPath("~/Content/Team.txt"),Encoding.Unicode);

            return View(team);
        }

        public ActionResult Video()
        {
            return View();
        }
    }
}
