package org.nmakhotkin.generator;

/**
 * Created with IntelliJ IDEA.
 * User: Kolyan
 * Date: 04.02.13
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */
public class PointD implements Cloneable {
    public Double x;
    public Double y;

    public PointD(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double getDist(PointD point){
        return Math.sqrt(Math.pow(point.x - x,2) + Math.pow(point.y - y,2));
    }

    @Override
    protected PointD clone() {
        return new PointD(x,y);
    }

    @Override
    public String toString() {
        return "X = "
                + x.toString()
                + "\nY = "
                + y.toString();
    }
}
