package org.nmakhotkin.generator;

/**
 * Created with IntelliJ IDEA.
 * User: Kolyan
 * Date: 02.03.13
 * Time: 16:59
 */
public class Generator {
    public double p = 0;
    public double h = 0;
    public double M = 0;
    public int cnt = 0;
    public double dist = 0;
    public Double dx = 0D, dy = 0D;
    public PointD from = new PointD(0D, 0D);
    public PointD to = new PointD(0D, 0D);

    public double f(double x) {
        if (x < p)
            return x;
        else
            return 2 * p - x;
    }

    public void CalcM() {
        double sum = 0;
        for (int i = 1; i <= cnt; i++) {
            sum += f(i * h);
        }
        M = dist / sum;
    }

    public double GetSpeedX(double Speed) {
        return Speed * (dx) / dist;
    }

    public double GetSpeedY(double Speed) {
        return Speed * (dy) / dist;
    }
}
