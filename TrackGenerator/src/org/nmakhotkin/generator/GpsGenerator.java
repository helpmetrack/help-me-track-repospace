package org.nmakhotkin.generator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.deploy.Environment;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * User: Kolyan
 * Date: 17.01.13
 */
public class GpsGenerator {
    private Transport transport = new Transport();
    private Double speed = 0.00002D;
    private int delay = 1000;
    private int cnt = 50;
    private PointD point1, point2, position;
    private Generator generator = new Generator();

    public void setDelay(int delay) {
        this.delay = delay;
    }

    private void changeCoords() {
        if (speed  < 0.000001D) {
            PointD tmp = point1.clone();
            point1 = point2.clone();
            point2 = tmp.clone();
            position = point1.clone();
            calculate();
            speed = 0.00002D;
            transport.load = String.valueOf(new Random().nextInt(100));
            return;
        }
        speed = generator.M * generator.f(vectorLength(generator.from, position));
        position.x += generator.GetSpeedX(speed);
        position.y += generator.GetSpeedY(speed);
    }

    protected double vectorLength(PointD v1, PointD v2) {
        return (float) Math.sqrt(Math.pow(v2.x - v1.x, 2) + Math.pow(v2.y - v1.y, 2));
    }

    private ActionListener setNewCoords = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            changeCoords();
        }
    };

    private Timer timer = new Timer(delay, setNewCoords);

    private void calculate() {
        generator.dist = vectorLength(point1, point2);
        generator.dx = point2.x - point1.x;
        generator.dy = point2.y - point1.y;
        generator.p = generator.dist / 2;
        generator.cnt = this.cnt;
        generator.h = 2 * generator.p / generator.cnt;
        generator.from.x = point1.x + generator.dx / generator.cnt;
        generator.from.y = point1.y + generator.dy / generator.cnt;
        position = generator.from.clone();
        generator.from = point1.clone();
        generator.to.x = point2.x;
        generator.to.y = point2.y;
        generator.CalcM();
    }

    public GpsGenerator(PointD point1, PointD point2) {
        this.point1 = point1;
        this.point2 = point2;
        position = point1.clone();
        calculate();
    }

    public GpsGenerator(File json) {
        readFromJSON(json);
        calculate();
    }

    // TODO post
    public void post(URI url) {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        try {
            mapper.writeValue(writer, getTransport());
        } catch (IOException e) {
            e.printStackTrace();
        }
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        StringEntity entity = null;
        try {
            entity = new StringEntity(writer.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        post.setEntity(entity);
        HttpResponse response = null;
        System.out.println("request: "+writer.toString());
        try {
            response = client.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Transport getTransport() {
        transport.LAT = position.x.toString();
        transport.LON = position.y.toString();
        /*List<Transport> routeList = new ArrayList<Transport>();
        routeList.add(transport);
        HashMap<String,List<Transport>> routes = new HashMap<String,List<Transport>>();
        routes.put(track.toString(),routeList);
        HashMap<String,HashMap<String,List<Transport>>> result = new HashMap<String, HashMap<String, List<Transport>>>();
        result.put("routes",routes);
        body.type = "taxi";
        body.result = result; */
        return transport;
    }

    public void start() {
        timer.start();
    }

    public void setTransport(int ID, int track,int load,String type) {
        transport.ID = String.valueOf(ID);
        transport.number = String.valueOf(track);
        transport.load = String.valueOf(load);
        transport.type = type;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        try {
            mapper.writeValue(writer, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public void readFromJSON(File json) {
        JsonObject transport = new JsonObject();
        try {
            transport = new JsonParser().parse(new FileReader(json)).getAsJsonObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.transport.type = transport.get("type").getAsString();
        this.transport.load = transport.get("load").getAsString();
        this.transport.number = transport.get("track").getAsString();
        this.transport.ID = transport.get("ID").getAsString();
        this.delay = transport.get("delay").getAsInt();
        this.cnt = transport.get("cnt").getAsInt();
        this.point1 = new PointD(0D,0D);
        this.point2 = new PointD(0D,0D);
        this.point1.x = transport.get("from").getAsJsonArray().get(0).getAsDouble();
        this.point1.y = transport.get("from").getAsJsonArray().get(1).getAsDouble();
        this.point2.x = transport.get("to").getAsJsonArray().get(0).getAsDouble();
        this.point2.y = transport.get("to").getAsJsonArray().get(1).getAsDouble();
        this.position = point1.clone();
        this.transport.LAT = point1.x.toString();
        this.transport.LON = point1.x.toString();
    }

    // internal test
    public static void main(String[] args) throws InterruptedException {
        URI url = null;
        try {
            url = new URI(args[0]);
        } catch (URISyntaxException e) {
            System.out.println("Invalid Url.\nUsage: Generator.jar <host-to-rest-server> <dir-with-configs>");
            System.exit(1);
        }
        File dir = new File(args[1]);
        if (!dir.isDirectory()) {
            System.out.println("Invalid Path. Is not directory");
            System.exit(2);
        }
        File[] fileList = dir.listFiles();
        if (fileList.length < 1) {
            System.out.println("Empty directory");
            System.exit(3);
        }
        List<GpsGenerator> generators = new ArrayList<GpsGenerator>();
        for (File file : fileList) {
            generators.add(new GpsGenerator(file));
        }
        for (GpsGenerator generator : generators) {
            generator.start();
        }
        /*generators.add(new GpsGenerator(new File("bus18.json")));
        generators.add(new GpsGenerator(new File("taxi44.json")));
        generators.add(new GpsGenerator(new File("taxi93.json")));
        generators.add(new GpsGenerator(new File("bus53.json")));
        */
        while (true) {
            for (GpsGenerator generator : generators) {
                generator.post(url);
            }
            Thread.sleep(generators.get(0).delay);
        }
    }
}
